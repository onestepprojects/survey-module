package org.onestep.relief.survey.exceptions;

public class SurveyServiceDBException extends Exception {

    public SurveyServiceDBException(String message) {
        this(message, null);
    }

    public SurveyServiceDBException(String message, Throwable cause) {
        super(message, cause);
    }
}