package org.onestep.relief.survey;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.onestep.relief.survey.db.SurveyDao;
import org.onestep.relief.survey.db.SurveyDaoImpl;
import org.onestep.relief.survey.db.SurveyResultDao;
import org.onestep.relief.survey.db.SurveyResultDaoImpl;
import org.onestep.relief.survey.model.SurveyResult;
import org.onestep.relief.survey.resources.SurveyServiceResource;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class OneStepReliefSurveyApplication extends Application<OneStepReliefSurveyConfiguration> {

    public static void main(final String[] args) throws Exception {
        new OneStepReliefSurveyApplication().run(args);
    }

    @Override
    public String getName() {
        return "OneStepReliefSurvey";
    }

    @Override
    public void initialize(final Bootstrap<OneStepReliefSurveyConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final OneStepReliefSurveyConfiguration configuration,
                    final Environment environment) {

        // Enable CORS headers
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        final SurveyDao surveyDao = new SurveyDaoImpl();
        final SurveyResultDao surveyResultDao = new SurveyResultDaoImpl();
        environment.jersey().register(new SurveyServiceResource(surveyDao, surveyResultDao));
    }

}
