package org.onestep.relief.survey.db;

import org.onestep.relief.survey.model.Survey;
import org.onestep.relief.survey.model.SurveyResult;

import java.util.List;

public interface SurveyResultDao {
    public List<SurveyResult> getSurveyResults(String surveyId);

    public void addResult(SurveyResult surveyResult);

    public void deleteResult(String surveyResultId);

    public void updateResult(String surveyResultId, SurveyResult surveyResult);

    public SurveyResult getResult(String surveyResultId);
}
