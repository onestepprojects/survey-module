package org.onestep.relief.survey.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.onestep.relief.survey.core.AppUtils;
import org.onestep.relief.survey.model.Survey;
import org.onestep.relief.survey.model.SurveyResult;
import org.onestep.relief.survey.model.SurveyStatus;

import javax.inject.Inject;
import java.util.*;

public class SurveyResultDaoImpl implements SurveyResultDao {

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;
    private DynamoDBMapperConfig dynamoDBMapperConfig;

    @Inject
    public SurveyResultDaoImpl() {
        this.client = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        this.dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(AppUtils.TABLE_NAME_SURVEY_RESULT)
                )
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .build();
        this.mapper = new DynamoDBMapper(client, this.dynamoDBMapperConfig);
    }

    @Override
    public List<SurveyResult> getSurveyResults(String surveyId) {
        // Build the attributes value to filter results
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":surveyIdValue", new AttributeValue().withS(surveyId));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("surveyId = :surveyIdValue")
                .withExpressionAttributeValues(eav);

        return this.mapper.scan(SurveyResult.class, scanExpression);
    }

    @Override
    public void addResult(SurveyResult surveyResult) {
        surveyResult.setDateSubmitted(new Date());
        this.mapper.save(surveyResult);
    }

    @Override
    public void deleteResult(String surveyResultId) {
        SurveyResult surveyResult = getResult(surveyResultId);
        this.mapper.delete(surveyResult);
    }

    @Override
    public void updateResult(String surveyResultId, SurveyResult surveyResult) {
        surveyResult.setDateSubmitted(new Date());
        this.mapper.save(surveyResult);
    }

    @Override
    public SurveyResult getResult(String surveyResultId) {
        return this.mapper.load(SurveyResult.class, surveyResultId);
    }
}
