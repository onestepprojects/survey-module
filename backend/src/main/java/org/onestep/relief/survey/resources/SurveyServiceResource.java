package org.onestep.relief.survey.resources;

import io.dropwizard.jersey.params.LongParam;
import org.onestep.relief.survey.db.SurveyDao;
import org.onestep.relief.survey.db.SurveyResultDao;
import org.onestep.relief.survey.exceptions.SurveyServiceException;
import org.onestep.relief.survey.model.Survey;
import org.onestep.relief.survey.model.SurveyResult;
import org.onestep.relief.survey.model.SurveyStatus;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/surveys")
@Produces(MediaType.APPLICATION_JSON)
public class SurveyServiceResource {

    private SurveyDao surveyDao;
    private SurveyResultDao surveyResultDao;

    public  SurveyServiceResource(SurveyDao surveyDao, SurveyResultDao surveyResultDao) {
        this.surveyDao = surveyDao;
        this.surveyResultDao = surveyResultDao;
    }

    @GET
    public Response getSurveys(@QueryParam("status") String status, @QueryParam("organizationId") String organizationId) {
        try {
            List<Survey> surveys = new ArrayList<>();
            if (status != null && organizationId == null) {
                SurveyStatus surveyStatus = SurveyStatus.valueOf(status);
                surveys = surveyDao.getSurveys(surveyStatus);
            } else if (status == null && organizationId != null) {
                surveys = surveyDao.getSurveys(organizationId);
            } else if (status != null && organizationId != null) {
                SurveyStatus surveyStatus = SurveyStatus.valueOf(status);
                List<Survey> surveysByOrg = surveyDao.getSurveys(organizationId);
                if (surveysByOrg != null && surveysByOrg.size() > 0) {
                    for (Survey surveyCurrent : surveysByOrg) {
                        if (surveyCurrent.getSurveyStatus() == surveyStatus) {
                            surveys.add(surveyCurrent);
                        }
                    }
                }
            } else {
                surveys = surveyDao.getSurveys();
            }
            return Response.status(Response.Status.OK).entity(surveys).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @POST
    public Response addSurvey(@Valid Survey survey) {
        try {
            Survey createdSurvey = surveyDao.addSurvey(survey);
            return Response.status(Response.Status.CREATED).entity(createdSurvey).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("/{surveyId}")
    public Response getSurvey(@PathParam("surveyId") String surveyId) {
        try {
            Survey survey = surveyDao.getSurvey(surveyId);
            if (survey == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey with the given Id does not exist")
                        .build();
            }

            return Response.status(Response.Status.OK).entity(survey).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("/{surveyId}/tags")
    public Response getSurveyTags(@PathParam("surveyId") String surveyId) {
        try {
            Survey survey = surveyDao.getSurvey(surveyId);
            if (survey == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey with the given Id does not exist")
                        .build();
            }
            List<String> tags = survey.getTags();
            return Response.status(Response.Status.OK).entity(tags).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Path("/{surveyId}/tags/{tag}")
    public Response addSurveyTag(@PathParam("surveyId") String surveyId, @PathParam("tag") String tag) {
        try {
            Survey survey = surveyDao.getSurvey(surveyId);
            if (survey == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey with the given Id does not exist")
                        .build();
            }
            if (surveyDao.addTag(surveyId, tag)) {
                String message = String.format("Tag '%s' has been added successfully!", tag);
                return Response.status(Response.Status.OK).entity(message).build();
            } else {
                String message = String.format("Tag '%s' was already available in the Survey!", tag);
                return Response.status(Response.Status.NOT_MODIFIED).entity(message).build();
            }
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{surveyId}/tags/{tag}")
    public Response removeSurveyTag(@PathParam("surveyId") String surveyId, @PathParam("tag") String tag) {
        try {
            Survey survey = surveyDao.getSurvey(surveyId);
            if (survey == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey with the given Id does not exist")
                        .build();
            }
            if (surveyDao.removeTag(surveyId, tag)) {
                String message = String.format("Tag '%s' has been removed successfully!", tag);
                return Response.status(Response.Status.OK).entity(message).build();
            } else {
                String message = String.format("Tag '%s' was not found in the survey!", tag);
                return Response.status(Response.Status.NOT_MODIFIED).entity(message).build();
            }
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{surveyId}")
    public Response deleteSurvey(@PathParam("surveyId") String surveyId) {
        try {
            Survey survey = surveyDao.getSurvey(surveyId);
            if (survey == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey with the given Id does not exist")
                        .build();
            }

            surveyDao.archiveSurvey(surveyId);
            String message = String.format("Survey with Id '%s' has been deleted successfully!", surveyId);
            return Response.status(Response.Status.OK).entity(message).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("/{surveyId}")
    public Response updateSurvey(@PathParam("surveyId") String surveyId, Survey survey) {
        try {
            Survey surveyExisting = surveyDao.getSurvey(surveyId);
            if (surveyExisting == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey with the given Id does not exist")
                        .build();
            }

            surveyDao.updateSurvey(surveyId, survey);
            Survey updatedSurvey = surveyDao.getSurvey(surveyId);
            return Response.status(Response.Status.OK).entity(updatedSurvey).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }


    @GET
    @Path("/{surveyId}/results")
    public Response getSurveyResults(@PathParam("surveyId") String surveyId) {
        try {
            // Check if Survey exists
            if (surveyDao.getSurvey(surveyId) == null)
                throw new SurveyServiceException("Survey with the given Id does not exist");

            List<SurveyResult> surveyResults = surveyResultDao.getSurveyResults(surveyId);
            return Response.status(Response.Status.OK).entity(surveyResults).build();
        }
        catch (SurveyServiceException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Path("/{surveyId}/results")
    public Response addSurveyResult(@PathParam("surveyId") String surveyId, SurveyResult result) {
        try {
            // Check if Survey exists
            if (surveyDao.getSurvey(surveyId) == null)
                throw new SurveyServiceException("Survey with the given Id does not exist");

            result.setSurveyId(surveyId);
            surveyResultDao.addResult(result);
            return Response.status(Response.Status.CREATED).entity(result).build();
        }
        catch (SurveyServiceException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @GET
    @Path("/{surveyId}/results/{surveyResultId}")
    public Response getSurveyResult(@PathParam("surveyId") String surveyId, @PathParam("surveyResultId") String surveyResultId) {
        try {
            // Check if Survey exists
            if (surveyDao.getSurvey(surveyId) == null)
                throw new SurveyServiceException("Survey with the given Id does not exist");

            SurveyResult result = surveyResultDao.getResult(surveyResultId);
            if (result == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Survey result with the given Id does not exist")
                        .build();
            }

            return Response.status(Response.Status.OK).entity(result).build();
        }
        catch (SurveyServiceException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{surveyId}/results/{surveyResultId}")
    public Response deleteSurveyResult(@PathParam("surveyId") String surveyId, @PathParam("surveyResultId") String surveyResultId) {
        try {
            // Check if Survey exists
            if (surveyDao.getSurvey(surveyId) == null)
                throw new SurveyServiceException("Survey with the given Id does not exist");

            // Check if Survey result exists
            if (surveyResultDao.getResult(surveyResultId) == null)
                throw new SurveyServiceException("Survey result with the given Id does not exist");

            surveyResultDao.deleteResult(surveyResultId);
            String message = String.format("Survey result with Id '%s' has been deleted successfully!", surveyResultId);
            return Response.status(Response.Status.OK).entity(message).build();
        }
        catch (SurveyServiceException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("/{surveyId}/results")
    public Response updateSurveyResult(@PathParam("surveyId") String surveyId, SurveyResult result) {
        try {
            // Check if Survey exists
            if (surveyDao.getSurvey(surveyId) == null)
                throw new SurveyServiceException("Survey with the given Id does not exist");

            // Check if Survey result exists
            if (surveyResultDao.getResult(result.getId()) == null)
                throw new SurveyServiceException("Survey result with the given Id does not exist");

            surveyResultDao.updateResult(result.getId(), result);
            SurveyResult updatedResult = surveyResultDao.getResult(result.getId());
            return Response.status(Response.Status.OK).entity(updatedResult).build();
        }
        catch (SurveyServiceException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
}
