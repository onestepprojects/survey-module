package org.onestep.relief.survey.db;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.onestep.relief.survey.core.AppUtils;
import org.onestep.relief.survey.exceptions.SurveyServiceDBException;
import org.onestep.relief.survey.model.Survey;
import org.onestep.relief.survey.model.SurveyStatus;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.*;

public class SurveyDaoImpl implements SurveyDao {

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;
    private DynamoDBMapperConfig dynamoDBMapperConfig;

    @Inject
    public SurveyDaoImpl() {
        this.client = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .build();
        this.dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(
                        DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(AppUtils.TABLE_NAME_SURVEY)
                )
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .build();
        this.mapper = new DynamoDBMapper(client, this.dynamoDBMapperConfig);
    }

    @Override
    public List<Survey> getSurveys(SurveyStatus status) {
        // Build the attributes value to filter results
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":surveyStatusValue", new AttributeValue().withS(status.toString()));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("surveyStatus = :surveyStatusValue")
                .withExpressionAttributeValues(eav);
        return this.mapper.scan(Survey.class, scanExpression);
    }

    @Override
    public List<Survey> getSurveys(String organizationId) {
        // Build the attributes value to filter results
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":organizationIdValue", new AttributeValue().withS(organizationId));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("organizationId = :organizationIdValue")
                .withExpressionAttributeValues(eav);
        return this.mapper.scan(Survey.class, scanExpression);
    }

    @Override
    public List<Survey> getSurveys() {
        // Build the attributes value to filter results
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":surveyStatusValue", new AttributeValue().withS(SurveyStatus.ARCHIVED.toString()));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("surveyStatus <> :surveyStatusValue")
                .withExpressionAttributeValues(eav);
        return this.mapper.scan(Survey.class, scanExpression);
    }

    @Override
    public List<Survey> getArchivedSurveys() {
        return getSurveys(SurveyStatus.ARCHIVED);
    }

    @Override
    public Survey addSurvey(Survey survey)
            throws SurveyServiceDBException {
        // Create a new Survey in case if it is not provided in the Body
        if (survey == null) {
            survey = new Survey();
        }

        // Set name if not present
        if (survey.getName() == null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String timestamp = dateFormat.format(new Date());
            survey.setName("Survey_" + timestamp);
        }

        if (survey.getTags().size() < 1) {
            survey.addTag(survey.getName());
        }
        this.mapper.save(survey);
        return survey;
    }

    @Override
    public void archiveSurvey(String surveyId)
            throws SurveyServiceDBException {
        Survey survey = getSurvey(surveyId);
        if (survey == null) {
            throw new SurveyServiceDBException("A Survey with the given Id does not exist");
        }
        survey.setSurveyStatus(SurveyStatus.ARCHIVED);
        survey.setDateLastModified(new Date());
        this.mapper.save(survey);
    }

    @Override
    public void updateSurvey(String surveyId, Survey survey)
            throws SurveyServiceDBException {
        Survey existingSurvey = getSurvey(surveyId);
        if (existingSurvey == null) {
            throw new SurveyServiceDBException("A Survey with the given Id does not exist");
        }
        survey.setDateLastModified(new Date());
        this.mapper.save(survey);
    }

    @Override
    public Survey getSurvey(String surveyId)
            throws SurveyServiceDBException {
        return this.mapper.load(Survey.class, surveyId);
    }

    @Override
    public List<String> getTags(String surveyId) throws SurveyServiceDBException {
        Survey survey = getSurvey(surveyId);
        if (survey == null) {
            throw new SurveyServiceDBException("A Survey with the given Id does not exist");
        }
        return survey.getTags();
    }

    @Override
    public boolean addTag(String surveyId, String tag) throws SurveyServiceDBException {
        Survey survey = getSurvey(surveyId);
        if (survey == null) {
            throw new SurveyServiceDBException("A Survey with the given Id does not exist");
        }
        boolean added = survey.addTag(tag);
        if (added) {
            survey.setDateLastModified(new Date());
            this.mapper.save(survey);
        }
        return added;
    }

    @Override
    public boolean removeTag(String surveyId, String tag) throws SurveyServiceDBException {
        Survey survey = getSurvey(surveyId);
        if (survey == null) {
            throw new SurveyServiceDBException("A Survey with the given Id does not exist");
        }
        boolean removed = survey.removeTag(tag);
        if (removed) {
            survey.setDateLastModified(new Date());
            this.mapper.save(survey);
        }
        return removed;
    }
}
