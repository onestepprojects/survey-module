package org.onestep.relief.survey.exceptions;

public class SurveyServiceAuthException extends Exception {

    public SurveyServiceAuthException(String message) {
        this(message, null);
    }

    public SurveyServiceAuthException(String message, Throwable cause) {
        super(message, cause);
    }
}

