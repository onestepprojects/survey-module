package org.onestep.relief.survey.exceptions;

public class SurveyServiceException extends Exception {

    public SurveyServiceException(String message) {
        this(message, null);
    }

    public SurveyServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}