package org.onestep.relief.survey.model;

public enum SurveyStatus {
    CREATED,
    ACCEPTING,
    PAUSED,
    FINALIZED,
    ARCHIVED
}
