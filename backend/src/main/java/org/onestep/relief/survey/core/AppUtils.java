package org.onestep.relief.survey.core;

public class AppUtils {

  public static final String TABLE_NAME_SURVEY_DEFAULT = "Survey";
  public static final String TABLE_NAME_SURVEY_RESULT_DEFAULT = "SurveyResult";

  public static final String TABLE_NAME_SURVEY = getEnv("TABLE_NAME_SURVEY", TABLE_NAME_SURVEY_DEFAULT);
  public static final String TABLE_NAME_SURVEY_RESULT = getEnv("TABLE_NAME_SURVEY_RESULT",
      TABLE_NAME_SURVEY_RESULT_DEFAULT);

  private static String getEnv(String key, String fallback) {
    if (System.getenv(key) == null) {
      return fallback;
    }
    return System.getenv(key);
  }
}
