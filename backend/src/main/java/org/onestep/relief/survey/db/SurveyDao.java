package org.onestep.relief.survey.db;

import org.onestep.relief.survey.exceptions.SurveyServiceDBException;
import org.onestep.relief.survey.model.Survey;
import org.onestep.relief.survey.model.SurveyStatus;

import java.util.List;

public interface SurveyDao {
    public List<Survey> getSurveys(SurveyStatus status);

    public List<Survey> getSurveys(String organizationId);

    public List<Survey> getSurveys();

    public List<Survey> getArchivedSurveys();

    public Survey addSurvey(Survey survey) throws SurveyServiceDBException;

    public void archiveSurvey(String surveyId) throws SurveyServiceDBException;

    public void updateSurvey(String surveyId, Survey survey) throws SurveyServiceDBException;

    public Survey getSurvey(String surveyId) throws SurveyServiceDBException;

    public List<String> getTags(String surveyId) throws SurveyServiceDBException;

    public boolean addTag(String surveyId, String tag) throws SurveyServiceDBException;

    public boolean removeTag(String surveyId, String tag) throws SurveyServiceDBException;
}
