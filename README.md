<!-- Publish revision: 2 -->

# Module : Survey Module

Survey Module for the OneStep Relief Platform

## Project Information

- [Module : Survey Module](#module--survey-module)
  - [Project Information](#project-information)
    - [Introduction](#introduction)
    - [Team Members](#team-members)
    - [Product Owners](#product-owners)
    - [Faculty](#faculty)
    - [Agile Coach](#agile-coach)
    - [Technical Details](#technical-details)
    - [Development Tools](#development-tools)
    - [Project Outline](#project-outline)
  - [UI Development](#ui-development)

### Introduction

### Team Members

- Eric Ding
- Eric S Moon
- Gustavo Varo
- Madhan Manoharan
- Michaela DeForest
- Oscar Anchondo

### Product Owners

- Nitya Timalsina
- Vijay Bhatt

### Faculty

- Annie Kamlang
- Eric Gieseke
- Hannah Riggs

### Agile Coach

- Hannah Riggs

### Technical Details

- Programming language(s)
  - Java
- API

### Development Tools

### Project Outline

## UI Development

The frontend portion of this project is contained within the `frontend/` directory. The project is set up as an npm module that can be packaged and pulled in by the OneStep UI Application. This module is also capable of functioning on it's own, in an effort to encourage modularity.

To begin developing the UI, you must have node installed and at least two terminal windows open.

1. The first terminal window will allow you to make changes to the project (under `frontend/src`) and have them reload in the browser window after they are packaged into `frontend/dist/`.
   Run `npm run start` from the `frontend/` directory and keep this running.

2. The second terminal window will be used for running the App server for the example app (under `frontend/example`). This "example" app pulls in our health module from the generated files as an npm package (see `frontend/example/App.js`), so that you can see the module in the browser. This example app will not be deployed with the module.
   Run `npm run start` from the `frontend/example` directory and keep this running as well.

All UI work should be done in the `frontend/src` directory. NPM packages can be installed from `frontend/` and used in the module.

Components are organized in the `frontend/src/components` directory by page and tests are located under `frontend/src/tests`. The Module is exported from `frontend/src/index/js` and this is the starting point.
