import Amplify, { API, graphqlOperation } from 'aws-amplify';
import awsconfig from './aws-exports';
Amplify.configure(awsconfig);

import { createTodo, updateTodo, deleteTodo } from './graphql/mutations';

const todo = { name: "My first todo", description: "Hello world!" };

/* create a todo */
await API.graphql(graphqlOperation(createTodo, { input: todo }));

/* update a todo */
await API.graphql(graphqlOperation(updateTodo, { input: { id: todoId, name: "Updated todo info" } }));

/* delete a todo */
await API.graphql(graphqlOperation(deleteTodo, { input: { id: todoId } }));

import { listTodos } from './graphql/queries';

const todos = await API.graphql(graphqlOperation(listTodos));

import { onCreateTodo } from './graphql/subscriptions';

// Subscribe to creation of Todo
const subscription = API.graphql(
    graphqlOperation(onCreateTodo)
).subscribe({
    next: (todoData) => {
        console.log(todoData);
        // Do something with the data
    }
});

// Stop receiving data updates from the subscription
subscription.unsubscribe();
