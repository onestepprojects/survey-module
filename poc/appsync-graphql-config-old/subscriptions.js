/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateSurvey = /* GraphQL */ `
  subscription OnCreateSurvey {
    onCreateSurvey {
      id
      surveyName
      author
      questions
      tag
      surveyResponses {
        items {
          id
          surveyID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSurvey = /* GraphQL */ `
  subscription OnUpdateSurvey {
    onUpdateSurvey {
      id
      surveyName
      author
      questions
      tag
      surveyResponses {
        items {
          id
          surveyID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSurvey = /* GraphQL */ `
  subscription OnDeleteSurvey {
    onDeleteSurvey {
      id
      surveyName
      author
      questions
      tag
      surveyResponses {
        items {
          id
          surveyID
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSurveyResponse = /* GraphQL */ `
  subscription OnCreateSurveyResponse {
    onCreateSurveyResponse {
      id
      surveyID
      survey {
        id
        surveyName
        author
        questions
        tag
        surveyResponses {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSurveyResponse = /* GraphQL */ `
  subscription OnUpdateSurveyResponse {
    onUpdateSurveyResponse {
      id
      surveyID
      survey {
        id
        surveyName
        author
        questions
        tag
        surveyResponses {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSurveyResponse = /* GraphQL */ `
  subscription OnDeleteSurveyResponse {
    onDeleteSurveyResponse {
      id
      surveyID
      survey {
        id
        surveyName
        author
        questions
        tag
        surveyResponses {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
