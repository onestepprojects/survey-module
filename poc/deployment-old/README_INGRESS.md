# Securing Kubernetes Service with SSL Certificate

This document explains how to secure a Kubernetes service using SSL Certificate.
We'll use **nginx** Ingress controller and [Let's Encrypt](https://letsencrypt.org/) for obtaining SSL certificate automatically using ACME protocol.

Prequisites:
- A working Kubernetes cluster with a **ClusterIP** Service deployed in it. This example uses **surveymodule** Service which is part of the 'Survey Module' deployment.
- [kubectl](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)
- DNS domain that you own. You can register a free domain with [Freenom](https://www.freenom.com/en/index.html?lang=en). In this example, we'll use the registered domain **onestepreliefsolutions.tk**.


**1. Setup NGINX Ingress controller**

   Here is the architecture of NGINX Ingress Controller
   
   ![NGINX Ingress Controller](./images/nginx_ingress.svg)

   1. Install [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/deploy/)
   ```shell
   kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.45.0/deploy/static/provider/cloud/deploy.yaml
   ```
   2. Check the status of the service and note down the external IP address. For this example, we'll call this **NGINX_SERVICE_IP**.
   ```shell
   kubectl get svc -n ingress-nginx
   ```
   3. Modify the external traffic policy 
   ```shell
   kubectl apply -f service_ingress_nginx.yaml
   ```
   
**2. Create an A record in your domain**
   1. In this example we have created an A record **survey** in our domain to point to the **NGINX_SERVICE_IP**.
   2. **IMPORTANT:** It may take several minutes for the DNS record changes to be applied.


**3. Install cert-manager**
   1. Create namespace
   ```shell
   kubectl create namespace cert-manager
   ```
   2. Install [cert-manager](https://cert-manager.io/docs/installation/kubernetes/)
   ```shell
   kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.3.0/cert-manager.yaml
   ```
   3. Check running pods
   ```shell
   kubectl get pods -n cert-manager
   ```

**4. Create ClusterIssuer**
   1. Open the file [issuer.yaml](issuer.yaml) and provide a valid email in line 10. For e.g. this could be the email id that you've used to register the domain.
   2. Create the issuer
   ```shell
   kubectl create -f issuer.yaml
   ```

**5. Create Ingress**
   1. Open the file [survey_ingress.yaml](survey_ingress.yaml) and provide valid host names in lines 11 & 14. In our example our domain base address is **onestepreliefsolutions.tk** and our A record is **survey**, so the host name will be **survey.onestepreliefsolutions.tk**.
   2. Apply the Ingress resource
   ```shell
   kubectl apply -f survey_ingress.yaml
   ```
   3. After a couple of minutes, check the status of the certificate. If everything went well, the status will be 'Certificate issued successfully'.
   ```shell
   kubectl describe certificate letsencrypt
   ```

**6. Check the Service**
Try to access the HTTPS endpoint and the browser should **NOT** complain about the certificate. In this example, try to access the URL: [https://survey.onestepreliefsolutions.tk](https://survey.onestepreliefsolutions.tk)
