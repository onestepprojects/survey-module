# Survey REST API on EKS

The Survey module backend is implemented with Dropwizard and delivered as a JAR file. This document explains how to deploy the backend as a Docker container in Amazon EKS.

## Prerequisites:
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [kubectl](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)
- [Docker](https://docs.docker.com/docker-for-mac/install/)
- Required IAM permissions - this example assumes that you have logged in as a root user.


1. Create your Amazon EKS cluster

   1. Create VPC and Subnets
   ```shell
    aws cloudformation create-stack \
        --stack-name surveymodule-vpc-stack \
        --template-url https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/amazon-eks-vpc-private-subnets.yaml \
        --tags Key=application,Value=onesteprelief-survey
    ```
   2. Creat IAM Role
   ```shell
   aws iam create-role \
    --role-name surveymoduleEKSClusterRole \
    --assume-role-policy-document file://"cluster-role-trust-policy.json"
   ```

   3. Attach the required Amazon EKS managed IAM policy to the role.
   ```shell
   aws iam attach-role-policy \
    --policy-arn arn:aws:iam::aws:policy/AmazonEKSClusterPolicy \
    --role-name surveymoduleEKSClusterRole
   ```

   4. Open the Amazon EKS console at https://console.aws.amazon.com/eks/home#/clusters. Make sure region is **'us-east-1'**.

   5. In the 'Configure Cluster' enter the information as shown in the image below. Name of the cluster in this example in **'surveymodule-cluster'**.
   ![Create Cluster](./images/create_cluster_01.PNG)

   6. In the 'Specify networking' page, select the VPC created in earlier step. Leave the other settings as default.
   ![Specify Networking](./images/create_cluster_02.PNG)

   7. In the 'Configure Logging' page, leave all settings as default.
   ![Configure Logging](./images/create_cluster_03.PNG)

   8. In the 'Review and create' page, check the settings and click 'Create'.
   ![Review and create](./images/create_cluster_04.PNG)

   9. It will take around 20 minutes to create the cluster.

1. Configure your computer to communicate with your cluster
   1. Create or update a kubeconfig file for your cluster.
    ```shell
   aws eks update-kubeconfig \
    --region us-east-1 \
    --name surveymodule-cluster
   ```

   2. Test your configuration.
   ```shell
   kubectl get svc
   ```
   3. Output
   ```
   NAME             TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
   svc/kubernetes   ClusterIP   10.100.0.1   <none>        443/TCP   1m
   ```

1. Create an IAM OpenID Connect (OIDC) provider

    1. Select the Configuration tab.
    2. In the Details section, copy the value for OpenID Connect provider URL.
    ![Details](./images/openid_01.PNG)
    3. Open the IAM console at https://console.aws.amazon.com/iam/.
    4. In the navigation panel, choose Identity Providers.
    5. Choose Add Provider.
    6. For Provider Type, choose OpenID Connect.
    7. For Provider URL, paste the OIDC provider URL for your cluster from step two, and then choose Get thumbprint.
    8. For Audience, enter sts.amazonaws.com and choose Add provider.
    ![Add provider](./images/openid_02.PNG)

1. Create Nodes

   1. Creat IAM roles
      1. In the file named [cni-role-trust-policy.json](cni-role-trust-policy.json), replace **<111122223333>** (including <>) with your account ID and replace **XXXXXXXXXX45D83924220DC4815XXXXX** with the value after the last **/** of your OpenID Connect provider URL.
      2. Create an IAM role for the Amazon VPC CNI plugin.
      ```shell
	  aws iam create-role \
        --role-name surveymoduleEKSCNIRole \
        --assume-role-policy-document file://"cni-role-trust-policy.json"
      ```
      3. Attach the required Amazon EKS managed IAM policy to the role.
      ```shell
	  aws iam attach-role-policy \
        --policy-arn arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy \
        --role-name surveymoduleEKSCNIRole
      ```
   2. Associate the Kubernetes service account used by the VPC CNI plugin to the IAM role. Replace <111122223333> (including <>) with your account ID.
   ```shell
    aws eks update-addon \
      --cluster-name surveymodule-cluster \
      --addon-name vpc-cni \
      --service-account-role-arn arn:aws:iam::<111122223333>:role/surveymoduleEKSCNIRole
   ```
   3. Create a node IAM role and attach the required Amazon EKS IAM managed policy to it.
      1. Create the node IAM role.
      ```shell
      aws iam create-role \
        --role-name surveymoduleEKSNodeRole \
        --assume-role-policy-document file://"node-role-trust-policy.json"
      ```
      2. Attach the required Amazon EKS managed IAM policies to the role.
      ```shell
      aws iam attach-role-policy \
        --policy-arn arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy \
        --role-name surveymoduleEKSNodeRole
      aws iam attach-role-policy \
        --policy-arn arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly \
        --role-name surveymoduleEKSNodeRole
      ```
    4. Open the Amazon EKS console at https://console.aws.amazon.com/eks/home#/clusters.
    5. Choose the name of the cluster that you created in Step 1: Create your Amazon EKS cluster, such as **surveymodule-cluster**.
    6. Select the Configuration tab.
    7. On the Configuration tab, select the Compute tab, and then choose Add Node Group.
    8. On the Configure node group page, fill out the parameters accordingly, accept the remaining default values, and then choose Next. In this example, Name is **ng-surveymodule** and IAM Role should be **surveymoduleEKSNodeRole**.
    ![Configure Node Group](./images/nodes_01.PNG)
    9. On the Set compute and scaling configuration page, accept the default values and select Next.
    ![Compute and Scaling](./images/nodes_02.PNG)
    10. On the Specify networking page, select an existing key pair to use for SSH key pair and then choose Next. If you don't have a key pair, you can create one with the following command. 
    ```shell
    aws ec2 create-key-pair --region us-east-1 --key-name surveymoduleKeyPair
    ```
    ![Specify networking](./images/nodes_03.PNG)
    11. On the Review and create page, review your managed node group configuration and choose Create.
    ![Review and create](./images/nodes_04.PNG)
    12. After several minutes, the Status in the Node Group configuration section will change from Creating to Active. Don't continue to the next step until the status is Active.


1. View resources

   1. In the left pane, select Clusters, and then in the list of Clusters, select the name of the cluster that you created, such as **surveymodule-cluster**.
   2. On the Overview tab, you see the list of Nodes that were deployed for the cluster. You can select the name of a node to see more information about it.
   ![View Nodes](./images/view_resources_01.PNG)
   3. On the Workloads tab of the cluster, you see a list of the workloads that are deployed by default to an Amazon EKS cluster. You can select the name of a workload to see more information about it.

## Create DynamoDB tables
1. Navigate to DynamoDB management console https://console.aws.amazon.com/dynamodb.
2. Create two tables **Survey** and **SurveyResult** with **id** as the partition key.
![View Nodes](./images/create_table_survey.png)
![View Nodes](./images/create_table_surveyresult.png)

## Setup ECR to host the Survey Module Docker image
Refer the official documentation to create a ECR: https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html

1. Authenticate to your default registry. Replace the 'aws_account_id' with your account ID.
```shell
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <aws_account_id>.dkr.ecr.us-east-1.amazonaws.com
```
2. Create a repository
```shell
aws ecr create-repository \
    --repository-name surveymodule \
    --image-scanning-configuration scanOnPush=true \
    --region us-east-1
```

3. In the file [dockerfile](dockerfile), set the value for the appropriate ENV variables.
You can get these values from the IAM management console https://console.aws.amazon.com/iam/.

![View Nodes](./images/access_key.PNG)

```shell
ENV AWS_ACCESS_KEY_ID=<YOUR_ACCESS_KEY_ID>
ENV AWS_SECRET_KEY=<YOUR_SECRET_KEY>
```

4. Now you need to build the 'survey-module' Dropwizard project. Navigate to the [../dropwizard-rest-api/survey-module/](../dropwizard-rest-api/survey-module/) folder and run:
```shell
mvn clean install
```
After building the project, copy the [survey-module-1.0.0-SNAPSHOT.jar](../dropwizard-rest-api/survey-module/target/survey-module-1.0.0-SNAPSHOT.jar) file to this folder.  

5. Build Docker image. Replace **aws_account_id** with your account id.
```shell
docker build -t aws_account_id.dkr.ecr.us-east-1.amazonaws.com/surveymodule:1.0.0 .
```

6. Push the image to ECR. Replace **aws_account_id** with your account id.
```shell
docker push aws_account_id.dkr.ecr.us-east-1.amazonaws.com/surveymodule:1.0.0
```

## Survey Module Deployment on EKS Cluster
1. In the deployment [surveymodule_deployment.yaml](surveymodule_deployment.yaml), set the appropriate Docker image in line 17.
2. Apply the deployment file
```shell
kubectl apply -f surveymodule_deployment.yaml
```
3. Check the public of the deployed service
```shell
kubectl get svc surveymodule
```
```
Output:

NAME    TYPE           CLUSTER-IP     EXTERNAL-IP                                                               PORT(S)          AGE
surveymodule   LoadBalancer   10.100.31.95   a2e8a6c21e55d401290fef5d8377fba0-1539196851.us-east-1.elb.amazonaws.com   80:31220/TCP   150m
```
4. The address in the EXTERNAL-IP column is the publicly exposed IP.
5. If everything works fine you should be able to access the service at **EXTERNAL-IP:80**.
