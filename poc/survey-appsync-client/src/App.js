import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';

import AWSAppSyncClient, { buildSubscription } from 'aws-appsync';
import { Rehydrated, graphqlMutation } from 'aws-appsync-react';
import awsmobile from './aws-exports';
import { ApolloProvider } from 'react-apollo';

import { graphql } from 'react-apollo';
import GetSurveys from './graphQL/GraphQLGetSurveys';
import NewSurvey from './graphQL/GraphQLCreateSurvey';
import 'bootstrap/dist/css/bootstrap.css';
import { Table } from 'react-bootstrap';


function App() {
  return (
    <div className="App">
      <AllSurveysWithData />
      <AddSurveyOffline />
    </div>
  );
}

class Surveys extends Component {
  render() {
    const { getSurveys, refetch } = this.props.data;
    return (
      <div>
        <button onClick={() => refetch()}>Refresh</button>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            {getSurveys && getSurveys.map((survey, index) => 
              <tr key={survey.id}>
                <td>{index}</td>
                <td>{survey.name}</td>
                <td>{survey.surveyStatus}</td>
              </tr>
            )}
          </tbody>
        </Table>
      </div>
    )
  }
}
const AllSurveysWithData = graphql(GetSurveys)(Surveys);


class AddSurvey extends Component {
  state = { name: '', description: '' }

  onChange(event, type) {
    this.setState({
      [type]: event.target.value
    })
  }

  render() {
    return (
      <div>
        <input onChange={(event) => this.onChange(event, "name")} />
        <input onChange={(event) => this.onChange(event, "description")} />
        <button onClick={() => this.props.createSurvey({
            name: this.state.name,
            description: this.state.description,
            author: 'Team 4'
          })}>
          Add
      </button>
      </div>
    );
  }
}
const AddSurveyOffline = graphqlMutation(NewSurvey, GetSurveys, 'String')(AddSurvey);

const client = new AWSAppSyncClient({
  url: awsmobile.aws_appsync_graphqlEndpoint,
  region: awsmobile.aws_appsync_region,
  auth: {
    type: awsmobile.aws_appsync_authenticationType,
    apiKey: awsmobile.aws_appsync_apiKey
  }
})

const WithProvider = () => (
  <ApolloProvider client={client}>
    <Rehydrated>
      <App />
    </Rehydrated>
  </ApolloProvider>
)

export default WithProvider;