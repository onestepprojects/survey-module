import gql from 'graphql-tag';

export default gql`
query {
  getSurveys {
    surveyStatus
    author
    definition
    description
    id
    isPublic
    name
    tags
    surveyStatus
  }
}`