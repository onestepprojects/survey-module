import gql from 'graphql-tag';

export default gql`
mutation($name: String $description: String $author: String) {
    createSurvey(survey: 
    {
        author: $author, 
        description: $description, 
        definition: "SurveyJS JSON"
        isPublic: false, 
        name: $name
    })
  }`