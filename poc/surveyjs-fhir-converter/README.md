# OneStepRelief - SurveyModule - SurveyJS FHIR Converter
# Java-based Lambda Functions


## Prerequisites

* AWS CLI
  * Configure AWS user credentials
  * Create S3 bucket
* Maven

## Project Creation
```bash
mvn archetype:generate -DgroupId=org.onestep.relief.healthcare \
                       -DartifactId=projectmanager \
                       -DarchetypeArtifactId=maven-archetype-simple \
                       -DarchetypeVersion=1.4 \
                       -DinteractiveMode=false
```

## Build
```bash
mvn clean package
```

## Deploy
* Create CloudFormation package
```bash
aws s3 mb s3://onesteprelief-surverymodule-converter-lambda-bucket
aws cloudformation package --template-file sam-template.yaml \
                           --output-template-file sam-template-output.yaml \
                           --s3-bucket onesteprelief-surverymodule-converter-lambda-bucket
```
* Deploy CloudFormation package
```bash
aws cloudformation deploy --template-file sam-template-output.yaml \
                          --stack-name onesteprelief-surveymodule-converter-api \
                          --capabilities CAPABILITY_IAM
```