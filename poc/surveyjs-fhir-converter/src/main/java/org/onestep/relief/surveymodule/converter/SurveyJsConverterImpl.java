package org.onestep.relief.surveymodule.converter;

public class SurveyJsConverterImpl implements SurveyJsConverter {

    @Override
    public String convertSurveyJsToFhirQuestionnaire(String surveyJs) {
        return "Converted to FHIR Questionnaire: " + surveyJs;
    }

    @Override
    public String convertFhirQuestionnaireToSurveyJs(String fhirQuestionnaire) {
        return "Converted to SurveyJS: " + fhirQuestionnaire;
    }
}
