package org.onestep.relief.surveymodule.converter;

public interface SurveyJsConverter {

    public String convertSurveyJsToFhirQuestionnaire(String surveyJs);
    public String convertFhirQuestionnaireToSurveyJs(String fhirQuestionnaire);
}
