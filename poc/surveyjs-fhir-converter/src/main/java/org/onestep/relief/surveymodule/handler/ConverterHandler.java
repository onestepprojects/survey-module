package org.onestep.relief.surveymodule.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.apache.http.HttpStatus;
import org.onestep.relief.surveymodule.converter.SurveyJsConverter;
import org.onestep.relief.surveymodule.converter.SurveyJsConverterImpl;

import java.util.HashMap;
import java.util.Map;

/*
    API for handling SurveyJS/FHIR converter
 */
public class ConverterHandler {

    private SurveyJsConverter converter;

    public SurveyJsConverter getConverter() {
        if (this.converter == null) {
            this.converter = new SurveyJsConverterImpl();
        }
        return this.converter;
    }

    public void setConverter(SurveyJsConverter converter) {
        this.converter = converter;
    }

    private Map<String, String> getDefaultHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Access-Control-Allow-Origin", "*");
        return headers;
    }

    public APIGatewayProxyResponseEvent convertToFhir(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String convertedJson = "";

        try {
            String surveyJsJson = request.getBody();
            convertedJson = getConverter().convertSurveyJsToFhirQuestionnaire(surveyJsJson);

        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(headers)
                .withBody(convertedJson);
    }

    public APIGatewayProxyResponseEvent convertToSurveyJs(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String convertedJson = "";

        try {
            String fhirJson = request.getBody();
            convertedJson = getConverter().convertFhirQuestionnaireToSurveyJs(fhirJson);

        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(headers)
                .withBody(convertedJson);
    }
}
