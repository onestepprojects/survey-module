# health-module

> OneStep Survey Module

[![NPM](https://img.shields.io/npm/v/survey-module.svg)](https://www.npmjs.com/package/health-module) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save survey-module
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'survey-module'
import 'survey-module/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [mdeforest](https://github.com/mdeforest)
