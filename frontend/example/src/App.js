import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { AuthHelper } from '@cscie599sec1spring21/authentication-helper'

import Amplify from 'aws-amplify'

import { Auth, Hub } from 'aws-amplify'

import amplifyConfig from './aws-amplify.config.js'

import Survey from 'survey-module'
import 'survey-module/dist/index.css'

import './shards-dashboards.1.1.0.min.css'

const App = () => {
  const [user, setUser] = useState(null)
  const [person, setPerson] = useState(null)

  Amplify.configure(amplifyConfig)

  useEffect(() => {
    const signIn = async () => {
      try {
        setUser(await getUser())
      } catch (error) {
        Auth.federatedSignIn()
      }
    }

    signIn()

    Hub.listen('auth', async ({ payload: { event, data } }) => {
      switch (event) {
        case 'signIn':
        case 'cognitoHostedUI':
          setUser(await getUser())
          break
        case 'signOut':
          setUser(null)
          break
        case 'signIn_failure':
        case 'cognitoHostedUI_failure':
        default:
          break
      }
    })
  }, [])

  // eslint-disable-next-line
  useEffect(async () => {
    if (!user) return

    let authHelper = new AuthHelper()

    const getPerson = async () => {
      var response = await authHelper.Get(
        process.env.REACT_APP_ORG_MODULE_API_URL + '/persons/requester/token'
      )

      if (response.status === 200 && response.data) {
        setPerson(response.data)
      }
    }

    getPerson()
  }, [user])

  const getUser = async () => {
    return await Auth.currentAuthenticatedUser()
  }

  const handleRenderProps = (routerProps) => {
    let authHelper = new AuthHelper()
    return (
      <Survey
        {...routerProps}
        authHelper={authHelper}
        person={person}
        mountPath={'/survey'}
      />
    )
  }

  return (
    <div>
      <Router>
        <Switch>
          <Route path='/survey' render={handleRenderProps} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
