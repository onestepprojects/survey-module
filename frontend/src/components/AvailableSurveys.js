import React, { useCallback, useState } from 'react'
import { Link } from 'react-router-dom-v5'
import {
  deleteSurvey,
  getSurveysByStatus,
  createSurveyDefinition,
  updateSurveyDefinition
} from './services/api/models/surveys'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

import { SurveyTypes } from './survey-grids/SurveyTypes'
import SurveyCard from './survey-grids/SurveyCard'

import style from './surveys.module.css'
import { useMutation, useQuery } from '@apollo/client'

const AvailableSurveys = ({ person }) => {
  // Get available surveys
  const [stateBins, setStateBins] = useState([
    {
      type: SurveyTypes.CREATED,
      accepts: [],
      surveys: null,
      // eslint-disable-next-line react/display-name
      buttons: (survey) => {
        return (
          <React.Fragment>
            <Link
              to={(location) => `${location.pathname}/creator?id=${survey.id}`}
              className={`btn btn-primary ${style.surveyButton}`}
            >
              Edit
            </Link>
            <button
              onClick={() => handleClone(survey.id, survey.surveyStatus)}
              type='button'
              className='btn btn-primary'
            >
              Clone
            </button>
            <button
              onClick={() => handleDelete(survey.id)}
              type='button'
              className='btn btn-primary'
            >
              Delete
            </button>
          </React.Fragment>
        )
      }
    },
    {
      type: SurveyTypes.ACCEPTING,
      accepts: [SurveyTypes.CREATED, SurveyTypes.PAUSED],
      surveys: null,
      // eslint-disable-next-line react/display-name
      buttons: (survey) => {
        return (
          <React.Fragment>
            <Link
              to={(location) => `${location.pathname}/${survey.id}/results`}
              className={`btn btn-primary ${style.surveyButton}`}
            >
              Results
            </Link>
            <Link
              to={(location) => `${location.pathname}/${survey.id}/results/new`}
              className={`btn btn-primary ${style.surveyButton}`}
            >
              Complete
            </Link>
            <button
              onClick={() => handleClone(survey.id, survey.surveyStatus)}
              type='button'
              className='btn btn-primary'
            >
              Clone
            </button>
          </React.Fragment>
        )
      }
    },
    {
      type: SurveyTypes.PAUSED,
      accepts: [SurveyTypes.ACCEPTING],
      surveys: null,
      // eslint-disable-next-line react/display-name
      buttons: (survey) => {
        return (
          <React.Fragment>
            <Link
              to={(location) => `${location.pathname}/${survey.id}/results`}
              className={`btn btn-primary ${style.surveyButton}`}
            >
              Results
            </Link>
            <button
              onClick={() => handleClone(survey.id, survey.surveyStatus)}
              type='button'
              className='btn btn-primary'
            >
              Clone
            </button>
          </React.Fragment>
        )
      }
    },
    {
      type: SurveyTypes.FINALIZED,
      accepts: [SurveyTypes.ACCEPTING, SurveyTypes.PAUSED],
      surveys: null,
      // eslint-disable-next-line react/display-name
      buttons: (survey) => {
        return (
          <React.Fragment>
            <Link
              to={(location) => `${location.pathname}/${survey.id}/results`}
            >
              <button type='button' className='btn btn-primary'>
                Results
              </button>
            </Link>
            <button
              onClick={() => handleDelete(survey.id)}
              type='button'
              className='btn btn-primary'
            >
              Archive
            </button>
            <button
              onClick={() => handleClone(survey.id, survey.surveyStatus)}
              type='button'
              className='btn btn-primary'
            >
              Clone
            </button>
          </React.Fragment>
        )
      }
    }
  ])

  const { loading: createdLoading, refetch: refetchCreated } = useQuery(
    getSurveysByStatus,
    {
      variables: { status: 'CREATED' },
      notifyOnNetworkStatusChange: true,
      onCompleted: (data) => {
        const newData = stateBins.map((bin, index) => {
          index === 0 && (bin.surveys = data.getSurveyByStatus)
          return bin
        })

        setStateBins(newData)
      }
    }
  )

  const { loading: acceptingLoading, refetch: refetchAccepting } = useQuery(
    getSurveysByStatus,
    {
      variables: { status: 'ACCEPTING' },
      notifyOnNetworkStatusChange: true,
      onCompleted: (data) => {
        const newData = stateBins.map((bin, index) => {
          index === 1 && (bin.surveys = data.getSurveyByStatus)
          return bin
        })

        setStateBins(newData)
      }
    }
  )

  const { loading: pausedLoading, refetch: refetchPaused } = useQuery(
    getSurveysByStatus,
    {
      variables: { status: 'PAUSED' },
      notifyOnNetworkStatusChange: true,
      onCompleted: (data) => {
        const newData = stateBins.map((bin, index) => {
          index === 2 && (bin.surveys = data.getSurveyByStatus)
          return bin
        })

        setStateBins(newData)
      }
    }
  )

  const { loading: finalizedLoading, refetch: refetchFinalized } = useQuery(
    getSurveysByStatus,
    {
      variables: { status: 'FINALIZED' },
      notifyOnNetworkStatusChange: true,
      onCompleted: (data) => {
        const newData = stateBins.map((bin, index) => {
          index === 3 && (bin.surveys = data.getSurveyByStatus)
          return bin
        })

        setStateBins(newData)
      }
    }
  )

  const [performDelete] = useMutation(deleteSurvey, {
    onCompleted: () => {
      refetchCreated()
      refetchAccepting()
      refetchPaused()
      refetchFinalized()
    }
  })

  const [createSurvey] = useMutation(createSurveyDefinition, {
    onCompleted: () => {
      refetchCreated()
    }
  })

  const [updateSurvey] = useMutation(updateSurveyDefinition, {
    onCompleted: () => {
      refetchCreated()
      refetchAccepting()
      refetchPaused()
      refetchFinalized()
    }
  })

  const handleDrop = useCallback(
    (dropLocation, item) => {
      const { entry, type } = item

      if (
        type === SurveyTypes.CREATED &&
        dropLocation.type === SurveyTypes.ACCEPTING
      ) {
        handleStartCampaign(entry.id, stateBins[0].surveys)
      } else if (
        type === SurveyTypes.PAUSED &&
        dropLocation.type === SurveyTypes.ACCEPTING
      ) {
        handleContinueCampaign(entry.id, stateBins[2].surveys)
      } else if (
        type === SurveyTypes.ACCEPTING &&
        dropLocation.type === SurveyTypes.PAUSED
      ) {
        handlePauseCampaign(entry.id, stateBins[1].surveys)
      } else if (
        type === SurveyTypes.ACCEPTING &&
        dropLocation.type === SurveyTypes.FINALIZED
      ) {
        handleEndCampaign(entry.id, stateBins[1].surveys)
      } else if (
        type === SurveyTypes.PAUSED &&
        dropLocation.type === SurveyTypes.FINALIZED
      ) {
        handleEndCampaign(entry.id, stateBins[2].surveys)
      }
    },
    [stateBins]
  )

  const handleDelete = (id) => {
    // Delete survey api call
    performDelete({
      variables: { id: id }
    })
  }

  const handleStartCampaign = (id, bins) => {
    // Change status from "CREATED" to "ACCEPTING"
    const survey = bins.find((s) => s.id === id)

    const cloneSurvey = { ...survey, surveyStatus: 'ACCEPTING' }

    delete cloneSurvey.__typename

    try {
      updateSurvey({
        variables: {
          id: survey.id,
          survey: cloneSurvey
        }
      })
    } catch (err) {
      //
    }
  }

  const handlePauseCampaign = (id, bins) => {
    // Change status from "ACCEPTING" to "PAUSED"
    const survey = bins.find((s) => s.id === id)

    const cloneSurvey = { ...survey, surveyStatus: 'PAUSED' }

    delete cloneSurvey.__typename

    try {
      updateSurvey({
        variables: {
          id: survey.id,
          survey: cloneSurvey
        }
      })
    } catch (err) {
      //
    }
  }

  const handleEndCampaign = (id, bins) => {
    // Change status from "ACCEPTING" to "FINALIZED"
    const survey = bins.find((s) => s.id === id)

    const cloneSurvey = { ...survey, surveyStatus: 'FINALIZED' }

    delete cloneSurvey.__typename

    try {
      updateSurvey({
        variables: {
          id: survey.id,
          survey: cloneSurvey
        }
      })
    } catch (err) {
      //
    }
  }

  const handleContinueCampaign = (id, bins) => {
    // Change status from "PAUSED" to "ACCEPTING"
    const survey = bins.find((s) => s.id === id)

    const cloneSurvey = { ...survey, surveyStatus: 'ACCEPTING' }

    delete cloneSurvey.__typename

    try {
      updateSurvey({
        variables: {
          id: survey.id,
          survey: cloneSurvey
        }
      })
    } catch (err) {
      //
    }
  }

  const handleClone = (id, curStatus) => {
    // Create new survey from current survey and post to db
    let survey

    switch (curStatus) {
      case 'CREATED':
        survey = stateBins[0].surveys.find((s) => s.id === id)
        break
      case 'ACCEPTING':
        survey = stateBins[1].surveys.find((s) => s.id === id)
        break
      case 'PAUSED':
        survey = stateBins[2].surveys.find((s) => s.id === id)
        break
      case 'FINALIZED':
        survey = stateBins[3].surveys.find((s) => s.id === id)
        break
    }

    const newSurvey = {
      author: person.name,
      definition: survey.definition,
      description: `Clone of ${survey.name}`,
      isPublic: false,
      name: `${survey.name}-Clone-${new Date().toISOString()}`
    }

    try {
      createSurvey({
        variables: { survey: newSurvey }
      })
    } catch (err) {
      //
    }
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <div className='main-content-container container-fluid px-4'>
        <div className='page-header row no-gutters py-4 mb-3 border-bottom'>
          <div className='col-12 col-sm-4 text-center text-sm-left mb-0'>
            <span className='text-uppercase page-subtitle'>Surveys</span>
            <h3 className='page-title'>Available Surveys</h3>
          </div>
        </div>
        <div className='d-flex flex-wrap'>
          {!createdLoading &&
            !acceptingLoading &&
            !pausedLoading &&
            !finalizedLoading &&
            stateBins.map((bin) => {
              return (
                <SurveyCard
                  key={bin.type}
                  data={bin.surveys}
                  cardType={bin.type}
                  accept={bin.accepts}
                  buttons={bin.buttons}
                  onDrop={(item) => handleDrop(bin, item)}
                />
              )
            })}
        </div>
        <Link to={(location) => `${location.pathname}/creator`}>
          <button type='button' className='btn btn-primary mt-3'>
            New Survey
          </button>
        </Link>
      </div>
    </DndProvider>
  )
}

export default AvailableSurveys
