import React, { useEffect, useState } from 'react'

import * as SurveyJSCreator from 'survey-creator'

import $ from 'jquery'

import './surveyjs.css'

import {
  createSurveyDefinition,
  getSurveyById,
  updateSurveyDefinition
} from './services/api/models/surveys'
import { useMutation, useQuery } from '@apollo/client'

const SurveyCreator = ({ location, person }) => {
  // TODO: Loader
  // TODO: Save Success Message
  // TODO: Styling
  // TODO: Back button

  const mainColor = '#007bff'
  const mainHoverColor = '#5a6169'
  const textColor = '#3d5170'

  const bootstrapThemeColorsEditor =
    SurveyJSCreator.StylesManager.ThemeColors.default
  bootstrapThemeColorsEditor['$primary-color'] = mainColor
  bootstrapThemeColorsEditor['$secondary-color'] = mainColor
  bootstrapThemeColorsEditor['$primary-hover-color'] = mainHoverColor
  bootstrapThemeColorsEditor['$primary-text-color'] = textColor
  bootstrapThemeColorsEditor['$selection-border-color'] = mainColor

  SurveyJSCreator.StylesManager.applyTheme()

  const [survey, setSurvey] = useState(null)
  const [creator, setCreator] = useState(null)

  const params = new URLSearchParams(location.search)
  const id = params.get('id')

  window.$ = window.jQuery = $

  const { loading: surveyLoading } = useQuery(getSurveyById, {
    variables: { id: id },
    notifyOnNetworkStatusChange: true,
    skip: !id,
    onCompleted: (data) => {
      setSurvey(data.getSurvey)
    }
  })

  const [updateSurvey] = useMutation(updateSurveyDefinition)
  const [createSurvey] = useMutation(createSurveyDefinition)

  useEffect(() => {
    if (!id) {
      setSurvey({
        author: person.name,
        definition: null,
        description: '',
        isPublic: false,
        name: ''
      })
    }
  }, [])

  useEffect(() => {
    if (surveyLoading || !survey) return

    const options = {
      haveCommercialLicense: true
    }

    const surveyCreator = new SurveyJSCreator.SurveyCreator(null, options)

    setCreator(surveyCreator)
  }, [survey])

  useEffect(() => {
    if (!creator) return
    creator.saveSurveyFunc = saveSurvey

    creator.showToolbox = 'right'
    creator.showPropertyGrid = 'right'
    creator.rightContainerActiveItem('toolbox')

    creator.render('surveyCreatorContainer')

    if (!survey.definition) return
    creator.text = survey.definition
  }, [creator])

  const saveSurvey = () => {
    if (!survey || !creator) return

    const surveyCopy = { ...survey, definition: creator.text }
    delete surveyCopy.__typename

    surveyCopy.name = JSON.parse(surveyCopy.definition).title
    surveyCopy.description = JSON.parse(surveyCopy.definition).description

    if (id) {
      try {
        updateSurvey({
          variables: { id: id, survey: surveyCopy }
        })
      } catch (err) {
        //
      }
    } else {
      try {
        createSurvey({
          variables: { survey: surveyCopy }
        })
      } catch (err) {
        //
      }
    }

    // Save successful
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom'>
        <div className='col-12 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Survey</span>
          <h3 className='page-title'>Survey Creator</h3>
        </div>
      </div>
      <div id='surveyCreatorContainer'></div>
    </div>
  )
}

export default SurveyCreator
