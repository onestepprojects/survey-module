export const SurveyTypes = {
  CREATED: 'Created',
  ACCEPTING: 'Accepting Responses',
  PAUSED: 'Paused',
  FINALIZED: 'Finalized'
}
