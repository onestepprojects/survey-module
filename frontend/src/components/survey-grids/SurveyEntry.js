import React, { memo } from 'react'
import { useDrag } from 'react-dnd'
import style from '../surveys.module.css'

const SurveyEntry = memo(({ entry, buttons, type }) => {
  const [{ opacity }, drag] = useDrag(
    () => ({
      type,
      item: { entry, type },
      collect: (monitor) => ({
        opacity: monitor.isDragging() ? 0.4 : 1
      })
    }),
    [entry, type]
  )

  return (
    <div
      ref={drag}
      role='Entry'
      className={`d-flex flex justify-content-between mb-3 border border-primary p-2 ${style.entry}`}
      style={{ opacity }}
    >
      <span className='text-left'>{entry.name}</span>
      <span className='text-right'>
        <div className='btn-group'>{buttons}</div>
      </span>
    </div>
  )
})

SurveyEntry.displayName = 'SurveyEntry'

export default SurveyEntry
