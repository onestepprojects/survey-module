import React, { memo } from 'react'

import { useDrop } from 'react-dnd'
import SurveyEntry from './SurveyEntry'

import style from '../surveys.module.css'

const SurveyCard = memo(({ data, buttons, cardType, accept, onDrop }) => {
  const [{ isOver, canDrop }, drop] = useDrop(() => ({
    accept,
    drop: onDrop,
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    })
  }))

  const isActive = isOver && canDrop

  let opacity = '1'
  if (isActive) {
    opacity = '0.4'
  }

  return (
    <div
      ref={(node) => drop(node)}
      role='Card'
      className={`mr-3 mb-3 mt-3 ${style.card}`}
    >
      <h6>{cardType}</h6>
      <div
        className={`card mt-3 w-100 ${style.surveyCard}`}
        style={{ opacity: opacity }}
      >
        <div
          className={`card-body d-flex flex-column m-1 ${style.surveyTable}`}
        >
          {data && data.length > 0 ? (
            data.map((survey) => {
              return (
                <SurveyEntry
                  key={survey.id}
                  entry={survey}
                  type={cardType}
                  buttons={buttons(survey)}
                />
              )
            })
          ) : (
            <div>
              <span>No Surveys Found</span>
            </div>
          )}
        </div>
      </div>
    </div>
  )
})

SurveyCard.displayName = 'SurveyCard'

export default SurveyCard
