import gql from 'graphql-tag'

export const addResult = gql`
  mutation AddSurveyResult($result: SurveyResultInput, $surveyId: String!) {
    addSurveyResult(result: $result, surveyId: $surveyId)
  }
`

export const getResultsById = gql`
  query GetResultsById($id: String!) {
    getSurveyResults(id: $id) {
      collector
      id
      result
      surveyId
    }
  }
`
