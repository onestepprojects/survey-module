import gql from 'graphql-tag'

export const getSurveysByStatus = gql`
  query GetSurveyByStatus($status: String) {
    getSurveyByStatus(status: $status) {
      author
      definition
      description
      id
      isPublic
      name
      organizationId
      surveyStatus
      tags
    }
  }
`

export const getSurveyById = gql`
  query GetSurveyById($id: String!) {
    getSurvey(id: $id) {
      id
      author
      definition
      description
      isPublic
      name
      surveyStatus
      tags
    }
  }
`

export const deleteSurvey = gql`
  mutation DeleteSurvey($id: String!) {
    deleteSurvey(id: $id)
  }
`

export const updateSurveyDefinition = gql`
  mutation UpdateSurvey($id: String!, $survey: SurveyInput) {
    updateSurvey(id: $id, survey: $survey)
  }
`

export const createSurveyDefinition = gql`
  mutation CreateSurvey($survey: SurveyInput) {
    createSurvey(survey: $survey)
  }
`
