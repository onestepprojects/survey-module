import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router-dom-v5'

import { Tabulator } from 'survey-analytics/survey.analytics.tabulator.js'
import { VisualizationPanel } from 'survey-analytics'
import * as Survey from 'survey-react'
import { Tabs, Tab } from 'react-bootstrap'

import jsPDF from 'jspdf'
import * as XLSX from 'xlsx'
import 'jspdf-autotable'

import { getResultsById } from './services/api/models/results'
import { getSurveyById } from './services/api/models/surveys'

import { useQuery } from '@apollo/client'

import './surveyjs.css'

window.jsPDF = jsPDF
window.XLSX = XLSX

const SurveyResults = () => {
  const { id } = useParams()

  const [survey, setSurvey] = useState(null)

  const { loading: resultsLoading, data: results } = useQuery(getResultsById, {
    variables: { id: id }
  })

  const { loading: surveyLoading } = useQuery(getSurveyById, {
    variables: { id: id },
    notifyOnNetworkStatusChange: true,
    onCompleted: (data) => {
      setSurvey(new Survey.SurveyModel(data.getSurvey.definition))
    }
  })

  useEffect(() => {
    if (
      resultsLoading ||
      surveyLoading ||
      results.getSurveyResults.length === 0
    )
      return

    const table = new Tabulator(
      survey,
      results.getSurveyResults.map((result) => JSON.parse(result.result))
    )

    table.haveCommercialLicense = true
    table.render(document.getElementById('tableContainer'))
  }, [survey, results])

  useEffect(() => {
    if (
      resultsLoading ||
      surveyLoading ||
      results.getSurveyResults.length === 0
    )
      return

    const visualization = new VisualizationPanel(
      survey.getAllQuestions(),
      results.getSurveyResults.map((result) => JSON.parse(result.result)),
      { haveCommercialLicense: true }
    )

    visualization.render(document.getElementById('visualizationsContainer'))
  }, [survey, results])

  return (
    <div className='main-content-container container-fluid px-4'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom'>
        <div className='col-12 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Survey</span>
          <h3 className='page-title'>Survey Results</h3>
        </div>
      </div>
      <Tabs defaultActiveKey='visualizations' variant='pills'>
        <Tab eventKey='visualizations' title='Visualizations'>
          <div id='visualizationsContainer' />
        </Tab>
        <Tab eventKey='table' title='Table'>
          <div id='tableContainer' />
        </Tab>
      </Tabs>
    </div>
  )
}

export default SurveyResults
