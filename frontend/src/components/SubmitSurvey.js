import { useMutation, useQuery } from '@apollo/client'
import React, { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom-v5'
import * as Survey from 'survey-react'
import { addResult } from './services/api/models/results'
import { getSurveyById } from './services/api/models/surveys'

import './surveyjs.css'

const SubmitSurvey = ({ mountPath, person }) => {
  const { id } = useParams()
  const [model, setModel] = useState(null)
  const [showCompleted, setShowCompleted] = useState(false)

  const history = useHistory()

  const mainColor = '#007bff'
  const mainHoverColor = '#5a6169'
  const textColor = '#3d5170'

  const bootstrapThemeColorsEditor = Survey.StylesManager.ThemeColors.default
  bootstrapThemeColorsEditor['$main-color'] = mainColor
  bootstrapThemeColorsEditor['$main-hover-color'] = mainHoverColor
  bootstrapThemeColorsEditor['$text-color'] = textColor
  bootstrapThemeColorsEditor['$header-color'] = mainColor
  bootstrapThemeColorsEditor['$header-background-color'] = textColor

  Survey.StylesManager.applyTheme()

  const { loading: surveyLoading, data: survey } = useQuery(getSurveyById, {
    variables: { id: id },
    notifyOnNetworkStatusChange: true,
    onCompleted: (data) => {
      const surveyModel = new Survey.Model(data.getSurvey.definition)
      surveyModel.showCompletedPage = false

      setModel(surveyModel)
    }
  })

  const [submitResult] = useMutation(addResult)

  const submitSurvey = (result) => {
    try {
      submitResult({
        variables: {
          result: {
            collector: person.name,
            result: JSON.stringify(result.data),
            surveyId: survey.getSurvey.id
          },
          surveyId: survey.getSurvey.id
        }
      })
    } catch (err) {
      //
    }
    setShowCompleted(true)
  }

  const resetSurvey = () => {
    model.clear()
    setShowCompleted(false)
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom'>
        <div className='col-12 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Survey</span>
          <h3 className='page-title'>Complete Survey</h3>
        </div>
      </div>
      {!showCompleted ? (
        model &&
        !surveyLoading && (
          <Survey.Survey
            model={model}
            onComplete={(data) => submitSurvey(data)}
          />
        )
      ) : (
        <div className='d-flex justify-content-center mt-5'>
          <div className='d-flex flex-column'>
            <h2>Thank you for Completing the Survey!</h2>
            <div className='d-flex justify-content-center mt-3'>
              <button
                className='btn btn-primary mr-3'
                onClick={() => history.push(mountPath)}
              >
                Go Back to Available Surveys
              </button>
              <button className='btn btn-primary' onClick={() => resetSurvey()}>
                Submit Another Result
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default SubmitSurvey
