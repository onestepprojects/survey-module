import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom-v5'

import * as SurveyJS from 'survey-react'

import awsmobile from './aws-exports'
import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  HttpLink,
  ApolloLink
} from '@apollo/client'
import { RetryLink } from '@apollo/client/link/retry'
import { setContext } from 'apollo-link-context'
import QueueLink from 'apollo-link-queue'
import SerializingLink from 'apollo-link-serialize'

import SurveyCreator from './components/SurveyCreator'
import AvailableSurveys from './components/AvailableSurveys'
import SurveyResults from './components/SurveyResults'

import 'chart.js'

import 'survey-react/survey.css'

import 'survey-creator/survey-creator.css'

import 'survey-analytics/survey.analytics.tabulator.css'
import 'survey-analytics/survey.analytics.css'
import 'tabulator-tables/dist/css/tabulator.min.css'

import SubmitSurvey from './components/SubmitSurvey'

const Survey = ({ match, authHelper, mountPath, person }) => {
  const [authLink, setAuthLink] = useState(null)
  const [client, setClient] = useState(null)

  const url = awsmobile.aws_appsync_graphqlEndpoint
  const httpLink = new HttpLink({ uri: url })
  const retryLink = new RetryLink()
  const queueLink = new QueueLink()
  const serializingLink = new SerializingLink()

  useEffect(() => {
    return () => {
      window.removeEventListener('offline', () => queueLink.close())
      window.removeEventListener('online', () => queueLink.open())
    }
  }, [])

  useEffect(() => {
    const getAuth = () => {
      const link = setContext(async (_, { headers }) => {
        // get the authentication token from local storage if it exists
        const token = await authHelper.GetUserAccessToken()
        // return the headers to the context so httpLink can read them
        return {
          headers: {
            ...headers,
            Authorization: token ? `${token}` : ''
          }
        }
      })

      setAuthLink(link)
    }

    getAuth()
  }, [])

  useEffect(() => {
    if (!authLink) return

    setClient(
      new ApolloClient({
        link: ApolloLink.from([
          authLink,
          queueLink,
          serializingLink,
          retryLink,
          httpLink
        ]),
        cache: new InMemoryCache()
      })
    )

    window.addEventListener('offline', () => {
      queueLink.close()
    })
    window.addEventListener('online', () => {
      queueLink.open()
    })
  }, [authLink])

  SurveyJS.StylesManager.applyTheme('bootstrap')

  return (
    client && (
      <ApolloProvider client={client}>
        <Router>
          <Switch>
            <Route
              exact
              path={`${match.path}/creator`}
              render={(props) => (
                <SurveyCreator
                  {...props}
                  auth={authHelper}
                  mountPath={mountPath}
                  person={person}
                />
              )}
            />
            <Route
              exact
              path={`${match.path}/:id/results/new`}
              render={(props) => (
                <SubmitSurvey
                  {...props}
                  auth={authHelper}
                  mountPath={mountPath}
                  person={person}
                />
              )}
            />
            <Route
              exact
              path={`${match.path}/:id/results`}
              render={(props) => (
                <SurveyResults
                  {...props}
                  auth={authHelper}
                  mountPath={mountPath}
                  person={person}
                />
              )}
            />
            <Route
              path={match.path}
              render={(props) => (
                <AvailableSurveys
                  {...props}
                  auth={authHelper}
                  mountPath={mountPath}
                  person={person}
                />
              )}
            />
          </Switch>
        </Router>
      </ApolloProvider>
    )
  )
}

export default Survey
